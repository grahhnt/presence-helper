const { app, BrowserWindow, Menu, Tray, ipcMain } = require("electron");
const path = require("path");
const rpc = require("discord-rpc");
const request = require("request");
const client = new rpc.Client({ transport: 'ipc' });

const VERSION = 1;
const endpoint = "https://presence.grahhnt.com";

app.setLoginItemSettings({
  openAtLogin: true
})
app.setName("sc07 Presence Helper");

const createWindow = () => {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    }
  })

  win.loadFile('app/index.html')

  win.on("close", () => {
    win = null;
  })
}

app.on("window-all-closed", () => {}); // do nothing

var checkFromServerInterval = null; // Interval
var lastUpdatedFromServer = null; // date
var lastDataFromServer = {};

const updateFromServer = (enable = true) => {
  win?.webContents.send("update-from-server-enable", enable);
  if(enable && !checkFromServerInterval) {
    const updateStatus = () => {
      request(endpoint + "/presence.json", {
        headers: {
          "Authorization": "DiscordUser " + client.user.id
        }
      }, (e, d, data) => {
        try {
          if(typeof data === "string") data = JSON.parse(data);
        }catch(e){
          console.error(endpoint + " offline");
          return;
        }
        if(e) {
          console.error(endpoint + " offline");
          return;
        }

        lastUpdatedFromServer = new Date();
        console.log("[Update From Server] Response:", data);
        lastDataFromServer = data;
        win?.webContents.send("server-data", {
          updatedAt: new Date(),
          ...data
        });

        if(data.none) {
          return;
        }
        data.buttons.forEach(b => {
          b.enable = true;
        })
        setActivity(data);
      })
    }
    checkFromServerInterval = setInterval(updateStatus, 1000 * 60);
    updateStatus();
  } else {
    clearInterval(checkFromServerInterval);
    checkFromServerInterval = null;
  }
}

ipcMain.on('setUpdateFromServer', (e, enable) => {
  updateFromServer(enable);
})

ipcMain.handle('getServerData', () => {
  return {
    enabled: !!checkFromServerInterval,
    lastUpdate: lastUpdatedFromServer,
    data: checkFromServerInterval ? lastDataFromServer : lastActivity
  }
})

ipcMain.on('clearActivity', () => {
  client.request('SET_ACTIVITY', {
    pid: process.pid
  })
});

var lastActivity = { buttons: [] };

const setActivity = ({
  details,
  state,
  largeImage,
  smallImage,
  buttons
}) => {
  lastActivity = {
    details,
    state,
    largeImage,
    smallImage,
    buttons
  }
  var assets = {};
  if(largeImage) assets.large_image = largeImage;
  if(smallImage) assets.small_image = smallImage;
  /*
  {
    large_image: largeImage,
    // large_text: config.LargeImageText,
    small_image: smallImage,
    // small_text: config.SmallImageText,
  }
   */

  const btns = buttons.filter(a => a.enable).map(btn => ({ label: btn.label, url: btn.url }));

  var activity = {
    assets
  }
  if(details) activity.details = details;
  if(state) activity.state = state;
  if(btns.length) activity.buttons = btns;

  client.request('SET_ACTIVITY', {
    pid: process.pid,
    activity
  }).catch(e => {
    win.webContents.send("saveError", e);
  })
}

ipcMain.on('setActivity', (event, data) => {
  setActivity(data);
});

ipcMain.handle('getDiscordUser', () => {
  return client.user;
})

ipcMain.handle('getVersion', () => VERSION);

const waitForDiscord = () => new Promise(done => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    closable: false
  });

  win.loadFile("app/connecting.html");

  client.login({ clientId : "775495935702204436" }).then(() => {
    console.log("connected")
    win.destroy();
    done();
    updateFromServer(true);
  }).catch(console.error);
})

var win;

app.whenReady().then(async () => {
  await waitForDiscord();

  const tray = new Tray(path.join(__dirname, "tray.ico"));

  tray.setContextMenu(Menu.buildFromTemplate([
    {
      label: "epic",
      click: () => {
        app.quit();
      }
    }
  ]))

  tray.on("click", () => {
    if(!win) {
      win = createWindow();
    } else {
      win.destroy();
      win = null;
    }
  })
})
